import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import lda
import warnings

warnings.simplefilter(action='ignore', category=FutureWarning)

# set output size
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

def get_cancer():
    # Load data
    breast_cancer = pd.read_csv("Datasets/breast_cancer.csv", sep=',',
                            names=['id', 'Clump_Thickness', 'Size', 'Shape', 'Marginal_Adhesion',
                                   'Epithelial_size', 'Bare_Nuclei', 'Bland_Chromatin', 'Normal_Nucleoli',
                                   'Mitoses', 'Class'])


    # Clean the data(delete data with "?" features)
    breast_cancer = breast_cancer[breast_cancer.Bare_Nuclei != '?']

    breast_cancer.Bare_Nuclei = breast_cancer.Bare_Nuclei.apply(int)

    # sort to drop duplicates
    breast_cancer.sort_values("id", inplace=True)
    # drop duplicates
    breast_cancer.drop_duplicates(keep='first', inplace=True)

    # drop ID
    breast_cancer = breast_cancer.drop(columns="id")

# Convert benign and malignant to 0 and 1, respectively
    condition = [(breast_cancer['Class'] == 2),(breast_cancer['Class'] == 4)]
    breast_cancer['Class'] = np.select(condition, [0, 1])



    return breast_cancer.as_matrix(columns=breast_cancer.columns[0:9]), breast_cancer.as_matrix(columns=breast_cancer.columns[9:])

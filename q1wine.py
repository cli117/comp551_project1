#!/usr/bin/env python
# coding: utf-8

import numpy as np
import pandas as pd
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)

pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

def drop_column(dataset, namesOfColumns):
    for name in namesOfColumns:
        dataset = dataset.drop(columns=name)
    return dataset

def add_column(dataset, newColumnName, namesOfColumns):
    dataset[newColumnName] = 1
    for name in namesOfColumns:
        dataset[newColumnName] = dataset[newColumnName] * dataset[name]
    return dataset

def get_redwine():
    # read the dataset into pandas
    red_wine = pd.read_csv("Datasets/winequality-red.csv", sep=';')
    # change column names(features) into a single word for conveince
    red_wine.rename(columns={'fixed acidity': 'fixed_acidity','citric acid':'citric_acid',
                         'volatile acidity':'volatile_acidity','residual sugar':'residual_sugar',
                         'free sulfur dioxide':'free_sulfur_dioxide','total sulfur dioxide':'total_sulfur_dioxide'}, 
                inplace=True)

    # see the first 10 rows to check if read correctly
    red_wine.head(n=10)


    # Clean the data. Are there any missing or malformed features?
    # Are there are other data oddities that need to bedealt with?
    # 1.check if there are some missing values
    red_wine.isnull().sum()


    addingColumns = {"sulphates", "alcohol"}
    red_wine = add_column(red_wine, "new_column", addingColumns)

    condition = [
        (red_wine['quality'] <= 5),
        (red_wine['quality'] >= 6)]
    red_wine['rating'] = np.select(condition, [0, 1])

    droppingColumns = {"quality", "residual_sugar"}
    red_wine = drop_column(red_wine, droppingColumns)

    boundary = len(red_wine.columns) - 1
    return red_wine.as_matrix(columns=red_wine.columns[0:boundary]), red_wine.as_matrix(columns=red_wine.columns[boundary:])

get_redwine()

import numpy as np
from q1wine import *
from q1breast import *
from lr import *
from lda import *


def evaluate_acc(q, labeldata):
    acc_num = 0
    numbel = labeldata.shape[0]
    for i in range(0, numbel):
        if q[i] == labeldata[i]:
            acc_num = acc_num+1
    acc_num = acc_num/numbel
    return acc_num


if __name__ == "__main__":
    print("testing logistic regression:")
    print("-----1.load data -----")
    x, y = get_redwine()
    x = np.hstack((x, np.ones((x.shape[0], 1), dtype=x.dtype)))
    feature_data = x[1:1000]
    label_data = y[1:1000]
    text_data = x[1000:]
    text_label_data = y[1000:]

    # x, y = get_cancer()
    # print(x.shape)
    # x = np.hstack((x, np.ones((x.shape[0], 1), dtype=x.dtype)))
    # feature_data = x[1:550]
    # label_data = y[1:550]
    # text_data = x[550:]
    # text_label_data = y[550:]

    print("-----2.training------")
    #clf = lr(alpha=0.0000946, max_cycle=500000)
    clf = lr(alpha=0.000001, max_cycle=509000)
    w = clf.fit(features=feature_data, label=label_data)
    print(w)
    print(w.shape)
    print("------3.model---------")
    h = clf.predict(text_data, w)
    print("-------4.accuracy value-----")
    acc = evaluate_acc(h, text_label_data)
    print(acc)
    # print("\n")



    #print("testing lda")

    #llda = lda(feature_data, label_data)
    #llda.fit(0, len(feature_data))
    #result = llda.predict(text_data)
    #print(evaluate_acc(result, text_label_data))




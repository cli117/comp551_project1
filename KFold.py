import numpy as np
from q1breast import *
from lr import *
from test import evaluate_acc
from lda import *
import random
import time

class KFold:

    @staticmethod
    def split(x, y, numOfChunks, indexOfTestSet):
        chunk_size = int(len(x) / numOfChunks)
        testCount = 0
        trainCount = 0
        for i in range(len(x)):
            if i > chunk_size*indexOfTestSet and i < chunk_size*(indexOfTestSet + 1):
                testCount += 1
            else:
                trainCount += 1

        trainX = np.zeros((trainCount, len(x[0])))
        trainY = np.zeros((trainCount, 1))
        testX = np.zeros((testCount, len(x[0])))
        testY = np.zeros((testCount, 1))

        trainIndex = 0
        testIndex = 0
        for i in range(len(x)):
            if i > chunk_size * indexOfTestSet and i < chunk_size * (indexOfTestSet + 1):
                for j in range(len(testX[0])):
                    testX[testIndex][j] = x[i][j]
                testY[testIndex] = y[i]
                testIndex += 1
            else:
                for j in range(len(trainX[0])):
                    trainX[trainIndex][j] = x[i][j]
                trainY[trainIndex] = y[i]
                trainIndex += 1
        print(trainX)

        return trainX, trainY, testX, testY

    def evaluate_acc(q, labeldata):
        acc_num = 0
        numbel = len(labeldata)
        for i in range(0, numbel):
            if q[i] == labeldata[i]:
                acc_num = acc_num + 1
        acc_num = acc_num / numbel
        return acc_num

    def kfold(x, y, numOfChunks, typeOfModel):
        accuracies = []
        timestamp = time.time()
        if typeOfModel == "lda":
            for i in range(numOfChunks):
                trainX, trainY, testX, testY = KFold.split(x, y, numOfChunks, i)
                klda = lda(trainX, trainY)
                klda.fit()
                result = klda.predict(testX)
                accuracies.append(KFold.evaluate_acc(result, testY))

        if typeOfModel == "lr":
            x = np.hstack((x, np.ones((x.shape[0], 1), dtype=x.dtype)))
            for i in range(numOfChunks):
                trainX, trainY, testX, testY = KFold.split(x, y, numOfChunks, i)
                clf = lr(alpha=0.005, max_cycle=50000)
                w = clf.fit(features=trainX, label=trainY)
                result = clf.predict(testX, w)
                accuracies.append(KFold.evaluate_acc(result, testY))
        timestamp = time.time() - timestamp
        print(timestamp)
        return accuracies



x, y = get_redwine()
x = (x - x.min()) / (np.ptp(x))
accuracy = KFold.kfold(x, y, 5, "lda")
# accuracy = KFold.kfold(x, y, 5, "lr")
print(accuracy)
print("Average: ")
print(np.average(accuracy))
